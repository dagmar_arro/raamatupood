-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: darro_raamatupood
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `darro_kasutajad`
--

DROP TABLE IF EXISTS `darro_kasutajad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `darro_kasutajad` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `eesnimi` varchar(30) NOT NULL,
  `perenimi` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `parool` varchar(20) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `darro_kasutajad`
--

LOCK TABLES `darro_kasutajad` WRITE;
/*!40000 ALTER TABLE `darro_kasutajad` DISABLE KEYS */;
INSERT INTO `darro_kasutajad` VALUES (18,'Sven','Eller','sven@gmail.com','sven',''),(19,'Sven','Eller','s@m.ee','sven',''),(20,'Sven','Eller','saaag@m.ee','sven','ADMIN');
/*!40000 ALTER TABLE `darro_kasutajad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `darro_kategooriad`
--

DROP TABLE IF EXISTS `darro_kategooriad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `darro_kategooriad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `darro_kategooriad`
--

LOCK TABLES `darro_kategooriad` WRITE;
/*!40000 ALTER TABLE `darro_kategooriad` DISABLE KEYS */;
INSERT INTO `darro_kategooriad` VALUES (1,'Ilukirjandus'),(2,'Laste -ja õppekirjandus'),(3,'Tehnika ja teadus'),(4,'Kasutatud raamatud');
/*!40000 ALTER TABLE `darro_kategooriad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `darro_raamatud`
--

DROP TABLE IF EXISTS `darro_raamatud`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `darro_raamatud` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `kid` int(6) NOT NULL,
  `nimi` varchar(50) NOT NULL,
  `hind` decimal(6,2) NOT NULL,
  `kirjeldus` longtext NOT NULL,
  `pilt` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `darro_raamatud`
--

LOCK TABLES `darro_raamatud` WRITE;
/*!40000 ALTER TABLE `darro_raamatud` DISABLE KEYS */;
INSERT INTO `darro_raamatud` VALUES (1,1,'Lõbus kala',3.89,'Vanniraamatuga koos on vanniskäik palju lõbusam. Ära unusta aeg-ajalt nuppu vajutada ja piiksutada!','Lobus kala. Vanniloomad.jpg'),(2,1,'Võõramaalane',24.77,'Dramaatiline sündmustik, ajalooline kontekst ja suured tunded.','Vooramaalane.jpg'),(3,2,'Nime vaev',23.05,'Meie unenäod ei ole paraku era-asjad, nad on tagasitulijaid täis.','nime vaev.jpg'),(4,2,'Niplisehted ja aksessuaarid',37.50,'Eesti-, veneja saksakeelses 143-leheküljelises kogumikus Niplisehted ja aksessuaarid II on 139 erineva ehte ja aksessuaari fotod ning mustrid, ei puudu ka praktilised nõuanded.','Niplisehted ja aksessuaarid.jpg'),(5,3,'Ma nüüd lähen',14.45,'Noore autori (s 1981) kolmas ilukirjanduslik teos.','Ma nuud lahen.jpg'),(10,1,'sadsa',0.00,'111','22.jpg');
/*!40000 ALTER TABLE `darro_raamatud` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-15 20:36:01
