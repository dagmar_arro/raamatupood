<?php
session_start();
session_destroy();
unset($_SESSION);
session_regenerate_id(true);
require_once 'config.php';
header('Location: '. $url);