<?php

require_once "funktsioonid.php";
if(isAdmin($_SESSION["user"])){
    if($_REQUEST['action'] == 'delete'){
        kustuta_toode($_GET['id']);
    }
    if($_REQUEST['action'] == 'lisa'){
        if(isset($_POST['kid']) && isset($_POST['nimi']) &&  isset($_POST['kirjeldus']) && isset($_POST['pilt']) && is_numeric($_POST['hind'])){
            lisa_toode($_POST);
        }else {
            $errors = "Kontrolli sisestatud andmeid!";
        }
    }
    $tooted = koik_tooted();
    require("views/admin.html");
}