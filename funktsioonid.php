<?php
require_once "config.php";
session_start();

function esilehe_tooted(){
    $conn = loo_yhendus();
    $sql = "select * from darro_raamatud limit 9";
    $resultset = $conn->query($sql);
    while ($data = $resultset->fetch_assoc())
    {
        $result[] = $data;
    }
    return $result;
}

function leia_toode($id){
    $conn = loo_yhendus();
    $id = $conn->real_escape_string($id);
    $sql = "select * from darro_raamatud where id = $id";
    $resultset = $conn->query($sql);
    return $resultset->fetch_assoc();
}

function kustuta_toode($id){
    $conn = loo_yhendus();
    $id = $conn->real_escape_string($id);
    $sql = "delete from darro_raamatud where id = $id";
    $conn->query($sql);
    $conn->close();
}

function lisa_toode($params){
    $conn = loo_yhendus();
    $kid = $conn->real_escape_string($params['kid']);
    $nimi = $conn->real_escape_string($params['nimi']);
    $kirjeldus = $conn->real_escape_string($params['kirjeldus']);
    $hind = $conn->real_escape_string($params['hind']);
    $pilt = $conn->real_escape_string($params['pilt']);
    $sql = "insert into darro_raamatud VALUES (NULL, '$kid','$nimi',  '$hind' , '$kirjeldus', '$pilt')";
    $conn->query($sql);

    $conn->close();
}

function lisa_ostukorvi($id){
    array_push($_SESSION['ostukorvi_tooted'], $id);
}

function ostukorvi_tooted(){
   $ostukorvi_tooted = array();
   foreach($_SESSION['ostukorvi_tooted'] as $id){
       array_push($ostukorvi_tooted, leia_toode($id));
   }
   return $ostukorvi_tooted;

}

function kategooria_tooted($kid){
    $conn = loo_yhendus();
    $kid = $conn->real_escape_string($kid);
    $sql = "select * from darro_raamatud where kid = $kid";
    $resultset = $conn->query($sql);
    fetch_all_assoc($resultset);
    $conn->close();
    return $resultset;
}

function isAdmin($user){
    $conn = loo_yhendus();
    $user = $conn->real_escape_string($user);
    $sql = "select count(*) from darro_kasutajad where darro_kasutajad.email = '$user' and role = 'ADMIN'";
    $result = $conn->query($sql);
    return $result->fetch_array()[0] > 0;
}

function koik_tooted(){
    $conn = loo_yhendus();
    $sql = "select * from darro_raamatud";
    $resultset = $conn->query($sql);
    $result = fetch_all_assoc($resultset);
    mysql_close($conn);
    return $result;
}

function kontrolli_parooli($kasutaja, $parool){
    $conn = loo_yhendus();

    $kasutaja = $conn->real_escape_string($kasutaja);
    $parool = $conn->real_escape_string($parool);

    $sql = "select count(*) from darro_kasutajad where darro_kasutajad.email = '$kasutaja' and parool = '$parool'";
    $result = $conn->query($sql);
    $success =  $result->fetch_array()[0] > 0;
    if($success){
        $_SESSION['user'] = $kasutaja;
        $_SESSION['ostukorvi_tooted'] = array();
    }
    $conn->close();
    return $success;
}

function registeeri_kasutaja($andmed){
    $conn = loo_yhendus();
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $eesnimi = $conn->real_escape_string($andmed['eesnimi']);
    $perenimi = $conn->real_escape_string($andmed['perenimi']);
    $email = $conn->real_escape_string($andmed['email']);
    $parool = $conn->real_escape_string($andmed['parool']);
    $sql = "insert into darro_kasutajad values(NULL, '$eesnimi', '$perenimi', '$email', '$parool', 'USER');";
    $conn->query($sql);
    mysqli_close($conn);
}

function leia_kategooriad(){
    $conn = loo_yhendus();
    $sql = "select * from darro_kategooriad";
    $resultset = $conn->query($sql);
    $retval = fetch_all_assoc($resultset);
    $conn->close();
    return $retval;
}

function fetch_all_assoc($resultset)
{
    $result = [];
    while ($data = $resultset->fetch_assoc()) {
        $result[] = $data;
    }
    return $result;
}


function loo_yhendus()
{
    $conn = new mysqli($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['password'], $GLOBALS['database']);
    mysqli_set_charset($conn, "utf8");
    return $conn;
}